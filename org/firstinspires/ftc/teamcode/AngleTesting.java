package org.firstinspires.ftc.teamcode;

import rover.ruckus.prototype.Prototype;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.hardware.modernrobotics.ModernRoboticsI2cCompassSensor;
import com.qualcomm.robotcore.hardware.Gyroscope;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.HardwareMap;

@Autonomous(name="Turn10xTest")
public class AngleTesting extends OpMode {

    private DcMotor left, right;

    @Override
    public void init() {  
        left = hardwareMap.get(DcMotor.class, "driveLeft");
        right = hardwareMap.get(DcMotor.class, "driveRight");
    }
    
    private boolean stop = false;
    
  @Override
    public void loop() {
        if(stop){
            left.setPower(0);
            right.setPower(0);
            return;
        }
        left.setPower(1);
        right.setPower(1);
      int leftEncoderPosition = left.getCurrentPosition();
      float rot = leftEncoderPosition / 7227f * 360;
      telemetry.addData("Rotations", rot);
        telemetry.addData("encoder", leftEncoderPosition);
      if(rot > 360)
        stop = true;
    }
}