package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;

@Autonomous (name="January")

public class AutonomousJanuary extends OpMode {
private DcMotor driveLeft, driveRight, /*armRight, */armLeft;
    @Override
    public void init(){
        driveLeft = hardwareMap.get(DcMotor.class,"driveLeft");
        driveRight = hardwareMap.get(DcMotor.class,"driveRight");  
        //armRight = hardwareMap.get(DcMotor.class,"armRight");
        armLeft = hardwareMap.get(DcMotor.class,"armLeft");
        armLeft.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        
        driveLeft.setDirection(DcMotor.Direction.FORWARD);
        driveRight.setDirection(DcMotor.Direction.REVERSE);
        
    }
    
    int counter = 0;
    @Override
    public void loop(){
      /*  if(counter < 90){
            armLeft.setPower(0.2f);
            counter++;
            return;
        }
        
        int leftRotation = driveLeft.getCurrentPosition();
        int rightRotation = driveRight.getCurrentPosition();
        if(leftRotation <= -4200){
            driveLeft.setPower(0);
            driveRight.setPower(0);
            return;
        }*/
        driveLeft.setPower(-0.7f);
        driveRight.setPower(-0.7f);
    }

        
}