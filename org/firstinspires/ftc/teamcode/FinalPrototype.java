package org.firstinspires.ftc.teamcode;

import rover.ruckus.prototype.Prototype;
import com.qualcomm.robotcore.hardware.Servo;
import java.util.logging.Logger;
import com.qualcomm.hardware.modernrobotics.ModernRoboticsI2cCompassSensor;
import com.qualcomm.robotcore.hardware.Gyroscope;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.HardwareMap;

import com.qualcomm.robotcore.eventloop.opmode.OpMode;

import rover.ruckus.prototype.*;

import rover.ruckus.prototype.*;

import org.firstinspires.ftc.teamcode.*;

public class FinalPrototype extends Prototype {

    // todo: write your code here
    
    public OpMode auto;
         
    private static final String COMPASS = "compass", LEFT = "driveLeft", RIGHT = "driveRight",LEFTARM="armLeft", ARM = "armRight",
    MARKER = "marker";
  
    public ModernRoboticsI2cCompassSensor compass;
    private DcMotor left, right, leftArm, rightArm;
    private Servo marker;
    
    private HardwareMap hardwareMap;
    
    public FinalPrototype(OpMode auto, HardwareMap hardwareMap){
        this.auto = auto;
        this.hardwareMap = hardwareMap;
        
        compass = hardwareMap.get(ModernRoboticsI2cCompassSensor.class, COMPASS);
        left = hardwareMap.get(DcMotor.class, LEFT);
        right = hardwareMap.get(DcMotor.class, RIGHT);
        
        leftArm = hardwareMap.get(DcMotor.class, LEFTARM);
        rightArm = hardwareMap.get(DcMotor.class, ARM);
        
        leftArm.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        rightArm.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        
        prevPositionLeft = left.getCurrentPosition();
        prevPositionRight = right.getCurrentPosition();
        
        prevAngle = (float)compass.getDirection();
        
        marker = hardwareMap.get(Servo.class, MARKER);
        marker.setPosition(0);
    }
    
    @Override
    public void lower(){
        leftArm.setPower(1);
        rightArm.setPower(1);
    }
    
    @Override
    public void stopArm(){
        leftArm.setPower(0);
        rightArm.setPower(0);
    }
    @Override
    public void raise(){
      leftArm.setPower(-1);
      rightArm.setPower(-1);
    }
    
    @Override
    public boolean placeMarker() {
        dcMotorPosition(marker, 90f);
        return true;
    }
    
    public void dcMotorPosition(Servo motor, float angle){
        motor.setPosition(angle / 180f);
    }

    public int prevPositionLeft = 0, prevPositionRight = 0;

    @Override
    public void stopMotorPower(){
        left.setPower(0);
        right.setPower(0);
    }
    
    private int frame = 0;
    
    @Override
    public float setMovePower(float power) {
        //Logger.addData("Hi", frame++);
        int leftRotation = left.getCurrentPosition();
        int rightRotation = right.getCurrentPosition();
        
        int deltaLeft = leftRotation - prevPositionLeft;
        int deltaRight = rightRotation - prevPositionRight;
        
        prevPositionLeft = leftRotation;
        prevPositionRight = rightRotation;
        
        left.setPower(-power);
        right.setPower(power);
        
        double dist = deltaLeft / 4000f * 100f;
        
        return (float)Math.abs(dist);
    }

    public float prevAngle = 0;

    //A 360 degree turn moves the left encoder -7227 encoder values
    @Override
    public float setTurnPower(float power) {
        //float ret = curDeg - prevAngle;
        
        //prevAngle = curDeg;
        
        
        left.setPower(-power);
        right.setPower(-power);
        
        int leftRotation = left.getCurrentPosition();
        int rightRotation = right.getCurrentPosition();
        
        int numExtraRotations = leftRotation - prevPositionLeft;
        
        prevPositionLeft = leftRotation;
        prevPositionRight = rightRotation;
        
        return -numExtraRotations / 7227f * 360f; //USE IF COMPASS DOES NOT WORK
        
        /*float delta = (float)(compass.getDirection() - prevAngle);
        prevAngle = (float)compass.getDirection();
        
        Logger.addData("Compass Angle", compass.getDirection());
        
        return delta;*/
    }

    @Override
    public void calibrate() {
        //nothing bc it is a compass sensor
        return;
    }

    @Override
    public void detach() {
        //TODO: later
    }

}