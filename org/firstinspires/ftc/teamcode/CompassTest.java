package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import org.firstinspires.ftc.robotcore.external.navigation.MagneticFlux;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.util.Range;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.hardware.modernrobotics.ModernRoboticsI2cCompassSensor;

@TeleOp

public class CompassTest extends OpMode{

private DcMotor driveLeft, driveRight, armRight, armLeft;
    public ModernRoboticsI2cCompassSensor compass;
    
    @Override
    public void init() {  
        compass = hardwareMap.get(ModernRoboticsI2cCompassSensor.class, "compass");
        driveLeft = hardwareMap.get(DcMotor.class,"driveLeft");
        driveRight = hardwareMap.get(DcMotor.class,"driveRight");  
        armRight = hardwareMap.get(DcMotor.class,"armRight");
        armLeft = hardwareMap.get(DcMotor.class,"armLeft");
        driveLeft.setDirection(DcMotor.Direction.FORWARD);
        driveRight.setDirection(DcMotor.Direction.REVERSE);
        armLeft.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
    }
    
    
  @Override
    public void loop() {
        double leftPower, rightPower;
        double drive = gamepad1.left_stick_y;
        double turn  =  -gamepad1.right_stick_x;
        leftPower    = Range.clip(drive + turn, -1.0, 1.0) ;
        rightPower   = Range.clip(drive - turn, -1.0, 1.0) ;
        driveLeft.setPower(leftPower);
        driveRight.setPower(rightPower);
        
        double armLeftD = 0.0, armRightD = 0.0;
        if(gamepad2.dpad_up){
            armLeftD = -1;
            armRightD = -1;
        }else if(gamepad2.dpad_down){
            armLeftD = 1;
            armRightD = 1;
        }
        
        armLeft.setPower(armLeftD);
        armRight.setPower(armRightD);
        
        float curDeg = (float) compass.getDirection();
        telemetry.addData("angle", curDeg);
        
        MagneticFlux flux = compass.getMagneticFlux();
        
        telemetry.addData("flux", flux.toString());
        
        
        int leftRotation = driveLeft.getCurrentPosition();
        int rightRotation = driveRight.getCurrentPosition();
        
        telemetry.addData("left", leftRotation);
        telemetry.addData("right", rightRotation);
        telemetry.update();
    }
    
}