package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.hardware.DcMotor;

@Autonomous(name="Drive1mTest")
public class DriveTesting extends OpMode {

    private DcMotor left, right;

    @Override
    public void init() {  
        left = hardwareMap.get(DcMotor.class, "driveLeft");
        right = hardwareMap.get(DcMotor.class, "driveRight");
    }
    
    private boolean stop = false;
    
  @Override
    public void loop() {
        if(stop){
            left.setPower(0);
            right.setPower(0);
            return;
        }
        left.setPower(1);
        right.setPower(-1);
      int leftEncoderPosition = left.getCurrentPosition();
      double dist = leftEncoderPosition / 4000f * 100f;
      if(dist > 100){
          stop = true;
      }
    }
}