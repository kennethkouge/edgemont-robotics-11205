package org.firstinspires.ftc.teamcode.teleopmodes;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import org.firstinspires.ftc.robotcore.external.navigation.Rotation;
import org.firstinspires.ftc.robotcore.external.Telemetry;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.util.ElapsedTime;
import com.qualcomm.robotcore.util.Range;

@TeleOp(name="TeleOpForTestingPurposes")

public class TestTeleOp extends OpMode {

    private DcMotor driveLeft, driveRight, armRight, armLeft;
    public static final String driveLeftString = "armLeft", driveRightString = "armRight",
    armLeftString = "driveLeft", armRightString = "driveRight";
    
    @Override
    public void init(){
        driveLeft = hardwareMap.get(DcMotor.class,driveLeftString);
        driveRight = hardwareMap.get(DcMotor.class,driveRightString);  
        armRight = hardwareMap.get(DcMotor.class,armRightString);
        armLeft = hardwareMap.get(DcMotor.class,armLeftString);
        driveLeft.setDirection(DcMotor.Direction.FORWARD);
        driveRight.setDirection(DcMotor.Direction.REVERSE);
        armLeft.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
    }
    @Override
    public void loop() {
        double leftPower, rightPower;
        double drive = gamepad1.left_stick_y;
        double turn  =  -gamepad1.right_stick_x;
        leftPower    = Range.clip(drive + turn, -1.0, 1.0) ;
        rightPower   = Range.clip(drive - turn, -1.0, 1.0) ;
        driveLeft.setPower(leftPower);
        driveRight.setPower(rightPower);
        

        double armLeftD = 0.0, armRightD = 0.0;
        if(gamepad2.dpad_up){
            armLeftD = -1;
            armRightD = -1;
        }else if(gamepad2.dpad_down){
            armLeftD = 1;
            armRightD = 1;
        }
        
        armLeft.setPower(armLeftD);
        armRight.setPower(armRightD);
    }
    
}