package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import org.firstinspires.ftc.robotcore.external.Telemetry;
import com.qualcomm.robotcore.hardware.HardwareMap;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;

import rover.ruckus.prototype.Main;
import rover.ruckus.prototype.*;

import org.firstinspires.ftc.teamcode.*;

/** Bottom right */

@Autonomous(name="Bottom Right (Red)")
public class AutoOpMode extends OpMode {
 public Main main;
 public int goldPos;
  
  public AutoOpMode(OpMode opMode, int goldPos){
    Logger.telemetry = opMode.telemetry;
    
    HardwareMap hardwareMap = opMode.hardwareMap;
      
      Prototype p = new FinalPrototype(opMode, hardwareMap);
      
      this.goldPos = goldPos;
      main = new Main(p, 25, -25, 360 - 45, Main.RED_BOTTOM, hardwareMap, goldPos);
  }
  
  @Override
    public void init() {  
/*      Logger.telemetry = telemetry;
      
      Prototype p = new FinalPrototype(this, hardwareMap);
      
      main = new Main(p, 25, -25, 360 - 45, Main.RED_BOTTOM, hardwareMap, goldPos);*/
    }
    
  @Override
    public void loop() {
      main.run();
    }
  
}