/*package simulator;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import main.Main;
import rover.ruckus.prototype.Prototype;

public class Start extends Application {

    public static void main(String[] args) throws Exception {
        launch(args);
    }
    
    public static final float WIDTH = 500, HEIGHT = 500;
    
    public Pane pane;
    public Circle robot;
    public boolean started = false;
    public Prototype p;
    public Main m;
    
    public float rot;
    
    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Rover Ruckus Simulator by Kenny Ge");
        StackPane root = new StackPane();
        
        root.getChildren().add(getImageView("rover_ruckus.png", WIDTH, HEIGHT));
        
        pane = new Pane();
        
        robot = new Circle(67.5 / 1080 * WIDTH);
        robot.setFill(new ImagePattern(new Image(Start.class.getResourceAsStream("robot.png"))));
        enableDrag(robot);
        
        pane.getChildren().add(robot);
        
        root.getChildren().add(pane);
        
        primaryStage.setScene(new Scene(root, WIDTH, HEIGHT));
        primaryStage.show();
        
        p = new SimulatorPrototype(this);
        
        root.getScene().setOnKeyPressed(
            (e)->{
            if(!started) {
                if(e.getCode().equals(KeyCode.LEFT)) {
                    rot += p.setTurnPower(-15);
                }
                if(e.getCode().equals(KeyCode.RIGHT)) {
                    rot += p.setTurnPower(+15);
                }
                while(rot < 0)
                    rot += 360;
                rot %= 360;
                
                System.out.println(rot);
            }
        });
        
        primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override public void handle(WindowEvent t) {
                Main.running = false;
                primaryStage.close();
            }
        });
    }
    
    public ImageView getImageView(String location, float width, float height) {
        javafx.scene.image.Image image = new javafx.scene.image.Image(Start.class.getResourceAsStream(location));
        javafx.scene.image.ImageView view = new javafx.scene.image.ImageView();
        view.setFitWidth(width);
        view.setFitHeight(height);
        view.setImage(image);
        
        return view;
    }
    
    public double deltaX, deltaY;
    public static final double ARENA = 365.76;
    
    private void enableDrag(final Circle circle) {
          circle.setOnMouseReleased((mouseEvent)->{
              circle.getScene().setCursor(Cursor.DEFAULT);
              double x = robot.getCenterX() / WIDTH * ARENA - ARENA / 2.0;
              double y = (robot.getCenterY() * -1) / WIDTH * ARENA + ARENA / 2.0;
              System.out.println("(" + x + ", " + y + ")");
              if(!started) {
                  started = true;
                  //System.out.println(robot.getCenterX() + " " + robot.getCenterY());
                  //(0,0) is in the center
                  while(rot < 0)
                      rot += 360;
                  rot %= 360;
                  
                  System.out.println("Angle: " + rot);
                  m = new Main(p, x, y, rot, Main.RED_TOP);
              }
          });
          circle.setOnMousePressed((mouseEvent)->{
              deltaX = (circle.getCenterX() - mouseEvent.getX());
              deltaY = (circle.getCenterY() - mouseEvent.getY());
              circle.getScene().setCursor(javafx.scene.Cursor.MOVE);
            });
            circle.setOnMouseDragged((mouseEvent)->{
                circle.setCenterX(mouseEvent.getX() + deltaX);
                circle.setCenterY(mouseEvent.getY() + deltaY);
            });
    }
    
}*/