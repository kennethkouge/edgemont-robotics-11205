package rover.ruckus.operations;

import rover.ruckus.prototype.Handler;
import rover.ruckus.prototype.Prototype;

public class SetAngle implements Operation {

    private float target, signum;
    public Prototype prototype;
    
    public SetAngle(float angle, Prototype prototype) {
        this.target = angle;
        this.prototype = prototype;
        signum = 1;
        //t = new Turn(angle - Handler.angleDeg, prototype);
        //Handler.addQueueObjects(buffered, t);
    }
    
    public float convert(float f) {
        while(f < 0)
            f += 360;
        f %= 360;
        return f;
    }
    
    private boolean everComplete = false;
    
    @Override
    public boolean complete() {
        boolean complete = aboutEquals(target, Handler.angleDeg);
        if(complete){
            prototype.stopMotorPower();
        }
        everComplete |= complete;
        return complete;
        //return t.complete();
    }

    @Override
    public void executeFrame() {
        //System.out.println(Handler.angleDeg + " " + target);
        if(!everComplete) {
            Handler.turnInternal(signum);
        }else{
            prototype.stopMotorPower();
        }
    }

    @Override
    public void add(Operation o) {
        return;
    }
    
    public boolean aboutEquals(float x, float y) {
        return Math.abs(x - y) <= 1;
    }

}
