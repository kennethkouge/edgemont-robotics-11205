package rover.ruckus.operations;

import rover.ruckus.prototype.*;

public class BlankOperation implements Operation {
    
    private Prototype prototype;

    public BlankOperation(Prototype prototype){
        this.prototype = prototype;
    }

    public int counter;

    public boolean complete(){
        return counter > 5;
    }
    
    public void executeFrame(){
        prototype.stopMotorPower();
        counter++;
    }
    
    public void add(Operation o){
        return;
    }
    
}