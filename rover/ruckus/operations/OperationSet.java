/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rover.ruckus.operations;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import rover.ruckus.prototype.*;

/**
 *
 * @author kenny ge
 * A set of simultaneous operations
 */
public class OperationSet implements Operation {
    
    public Map<Class<? extends Operation>, Operation> operations;

    public OperationSet() {
        operations = new HashMap<>();
    }
    
    public void add(Operation o){
        if(operations.containsKey(o.getClass())) {
            operations.get(o.getClass()).add(o);
        }else {
            operations.put(o.getClass(), o);
        }
    }

    @Override
    public boolean complete() {
        return operations.isEmpty();
    }

    @Override
    public void executeFrame() {
        if(complete())
            return;
        
        Iterator<Operation> i = operations.values().iterator();
        
        while(i.hasNext()) {
            Operation o = i.next();
/*            if(o instanceof Turn) {
                System.out.println("Turn");
            }*/
            Logger.addData("Operation", o.getClass().toString());
            if(!o.complete()) {
                o.executeFrame();
            }else{
                i.remove();
            }
        }
    }
    
}
