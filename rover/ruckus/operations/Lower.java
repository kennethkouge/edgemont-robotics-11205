package rover.ruckus.operations;

import rover.ruckus.prototype.*;

public class Lower implements Operation {
    
    private Prototype prototype;
    public int counter = 0;
    
    public Lower(Prototype prototype){
        this.prototype = prototype;
    }
    
    @Override
    public boolean complete(){
        return counter > 60 * 4;
    }
    
    @Override
    public void executeFrame(){
        if(complete()){
            prototype.stopArm();

            return;
        }
        prototype.lower();
        counter++;
    }
    
    @Override
    public void add(Operation o){
        return;
    }
    
}