/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rover.ruckus.operations;

import java.util.ArrayDeque;
import java.util.Deque;

/**
 *
 * @author kenny ge
 */
public class OperationSequence implements Operation {

    public Deque<OperationSet> queue;
    
    public OperationSequence(){
        queue = new ArrayDeque<>();
    }
    
    public boolean isEmpty() {
    	return queue.isEmpty();
    }

    @Override
    public boolean complete() {
        return queue.isEmpty();
    }

    @Override
    public void executeFrame() {
        if(!complete()){
            queue.peek().executeFrame();
            if(queue.peek().complete()){
                queue.remove();
            }
        }
    }

	@Override
	public void add(Operation o) {
		if(o instanceof OperationSet)
			queue.add((OperationSet)o);
		else
			System.err.println("Attempted to add standard operation to list of operation sets!");
	}
    
}
