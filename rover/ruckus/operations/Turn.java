/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rover.ruckus.operations;

import rover.ruckus.prototype.Handler;
import rover.ruckus.prototype.Prototype;

import rover.ruckus.prototype.*;

/**
 *
 * @author kenny ge
 */
public class Turn implements Operation {

    public Prototype p;
    public float currentAngle, deltaAngle;
    
    public Turn(float deltaAngle, Prototype p){
        this.deltaAngle = deltaAngle;
        this.p = p;
    }
    
    public boolean everComplete = false;

    /** @returns - whether or not the magnitude of the angle difference
     is equal to the desired magnitude. Note that direction is determined in the
     executeFrame function*/
    @Override
    public boolean complete() {
        Logger.addData("Angle", currentAngle);
        boolean complete = Math.abs(currentAngle) >= Math.abs(deltaAngle);
        if(complete)
            p.stopMotorPower();
        everComplete |= complete;
        return complete;
    }

    @Override
    public void executeFrame() {
        //System.out.println(Handler.angleDeg);
        //System.out.println("hi");
        Logger.addData("Turning", currentAngle);
        if(!everComplete){
            float delta = Math.abs(Handler.turnInternal(Math.signum(deltaAngle)));
            currentAngle += delta;
        }else{
            p.stopMotorPower();
        }
    }

    @Override
    public void add(Operation o) {
        Turn t = (Turn) o;
        deltaAngle += t.deltaAngle;
    }
    
}
