package rover.ruckus.operations;

import com.qualcomm.robotcore.hardware.HardwareMap;
import org.firstinspires.ftc.robotcore.external.tfod.Recognition;
import org.firstinspires.ftc.robotcore.external.ClassFactory;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import java.util.List;
import org.firstinspires.ftc.robotcore.external.ClassFactory;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer.CameraDirection;
import org.firstinspires.ftc.robotcore.external.tfod.TFObjectDetector;
import org.firstinspires.ftc.robotcore.external.tfod.Recognition;
import com.qualcomm.robotcore.hardware.HardwareMap;
import rover.ruckus.operations.MoveToPosition;
import rover.ruckus.operations.PlaceMarkerOperation;
import rover.ruckus.operations.SetAngle;
import rover.ruckus.prototype.Handler;
import rover.ruckus.prototype.Prototype;

import rover.ruckus.prototype.*;

public class MoveGoldOperation implements Operation {

    private Prototype prototype;
    private HardwareMap hardwareMap;
    
    private static final String TFOD_MODEL_ASSET = "RoverRuckus.tflite";
    private static final String LABEL_GOLD_MINERAL = "Gold Mineral";
    private static final String LABEL_SILVER_MINERAL = "Silver Mineral";
    private VuforiaLocalizer vuforia;
    private TFObjectDetector tfod;
    
    public static final String KEY = "Ac2dH3v/////AAABmXnujNLwWET1jV6h2oMTxmR"
  +"qw5GBwh3qWVXmWIG47cUEr4jzZVlDqLL8FXqzKaTp6HYlyaUbe/hwUJnbGTXIxag/QAu1eZvawWl7WDuA44J"
  +"4gKgvpU3P05k/LIH59244uMjJxeCLk77lID8PLqZr8qgPePnZ69HUo+gnFdFmhaMrfF5qsNDkCRjr7ZhIYQFKE"
  +"nwQfNt5OoPUpSZ1KyhEGyhJBgmZ3o3uGvbwF7V2l4LjM/RbFUlm5Aut2Yv03KDPm7A6LBbLLx3ttKMW6E0UYyyH3Z8SI2U"
  +"7Cm4Qr2OPfTQNKgKISMuBtGDl3ok4MucxClIpcYyULOYd+sZMHGFAL3GjMOphEyjag4jZIYvllDb0";
    
    private boolean done = false;

    public MoveGoldOperation(Prototype prototype, HardwareMap hardwareMap){
        this.prototype = prototype;
        this.hardwareMap = hardwareMap;
    }

    @Override
    public boolean complete(){
      if(done){
        if (tfod != null) {
            tfod.shutdown();
            tfod = null;
        }
      }
        return done;
    }
    
    boolean detectedObject = false, init = false;
    int pos = -1; 
    long start = System.currentTimeMillis();
    
    @Override
    public void executeFrame(){
      if(done)
        return;
      if(!init){
        initVuforia();

        initTfod();
      
        if (tfod != null) {
            tfod.activate();
        }
        init = true;
        return;
      }

        
            if (tfod != null) {
                    List<Recognition> updatedRecognitions = tfod.getUpdatedRecognitions();
                    if (updatedRecognitions != null) {
                      Logger.addData("# Object Detected", updatedRecognitions.size());
                      if (updatedRecognitions.size() == 3 && !detectedObject) {
                        int goldMineralX = -1;
                        int silverMineral1X = -1;
                        int silverMineral2X = -1;
                        for (Recognition recognition : updatedRecognitions) {
                          if (recognition.getLabel().equals(LABEL_GOLD_MINERAL)) {
                            goldMineralX = (int) recognition.getLeft();
                          } else if (silverMineral1X == -1) {
                            silverMineral1X = (int) recognition.getLeft();
                          } else {
                            silverMineral2X = (int) recognition.getLeft();
                          }
                        }
                        if (goldMineralX != -1 && silverMineral1X != -1 && silverMineral2X != -1) {
                          if (goldMineralX < silverMineral1X && goldMineralX < silverMineral2X) {
                            Logger.addData("Gold Mineral Position", "Left");
                            pos = 0;
                            detectedObject = true;
                          } else if (goldMineralX > silverMineral1X && goldMineralX > silverMineral2X) {
                            Logger.addData("Gold Mineral Position", "Right");
                            detectedObject = true;
                            pos = 2;
                          } else {
                            Logger.addData("Gold Mineral Position", "Center");
                            detectedObject = true;
                            pos = 1;
                          }
                        }
                      }
                      if(detectedObject && !done){
                        //Handler.move
                      }
                    }
                }
        
        if(pos == 0){
            Handler.addQueueObjectsSimultaneous(new Turn(-40, prototype));
        }else if(pos == 2){
          Handler.addQueueObjectsSimultaneous(new Turn(40, prototype));
        }
        
        if(detectedObject)
          done = true;
    }
    
    @Override
    public void add(Operation o){
        
    }
    
    private void initVuforia() {
      com.vuforia.Vuforia.init();
        VuforiaLocalizer.Parameters parameters = new VuforiaLocalizer.Parameters();
        parameters.vuforiaLicenseKey = KEY;
        parameters.cameraDirection = CameraDirection.BACK;
        vuforia = ClassFactory.getInstance().createVuforia(parameters);
    }
    private void initTfod() {
        int tfodMonitorViewId = hardwareMap.appContext.getResources().getIdentifier(
            "tfodMonitorViewId", "id", hardwareMap.appContext.getPackageName());
        TFObjectDetector.Parameters tfodParameters = new TFObjectDetector.Parameters(tfodMonitorViewId);
        tfod = ClassFactory.getInstance().createTFObjectDetector(tfodParameters, vuforia);
        //tfod.init();
        tfod.loadModelFromAsset(TFOD_MODEL_ASSET, LABEL_GOLD_MINERAL, LABEL_SILVER_MINERAL);
    }
}