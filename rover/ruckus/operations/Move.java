/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rover.ruckus.operations;

import rover.ruckus.prototype.Handler;
import rover.ruckus.prototype.Prototype;

import rover.ruckus.prototype.*;

/**
 *
 * @author kenny ge
 */
public class Move implements Operation {

    float originalX = 0, originalY;
    float distTravelled, target;
    public Prototype p;
    
    public Move(float dist, Prototype p){
        this.target = dist;
        this.p = p;
    }
    
    public boolean everComplete = false;
    
    @Override
    public boolean complete() {
        Logger.addData("dist", distTravelled);
        Logger.addData("target", target);
        everComplete |= (distTravelled >= target);
        return distTravelled >= target;
    }

    @Override
    public void executeFrame() {
        if(!everComplete){
            float delta = p.setMovePower(1);
            distTravelled += delta;
            Handler.updatePolarCartesianDelta(delta);
        }else{
            p.stopMotorPower();
        }
    }

    @Override
    public void add(Operation o) {
        Move m = (Move) o;
        target += m.target;
    }
    
}
