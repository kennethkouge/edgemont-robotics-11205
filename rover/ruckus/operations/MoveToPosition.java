package rover.ruckus.operations;

import rover.ruckus.prototype.Handler;
import rover.ruckus.prototype.Prototype;

public class MoveToPosition implements Operation {

    //private Move move;
    //private Turn turn;
    
    private float targetAngle, x, y, targetDist, currentDist;
    
    private Prototype prototype;
    
    public boolean angleCalculated = false;
    
    public MoveToPosition(float x, float y, Prototype prototype){
        this.prototype = prototype;
        this.x = x;
        this.y = y;
        
        //turn = new Turn(angle - Handler.angleDeg, prototype);
        //move = new Move(pythag(deltaX, deltaY), prototype);
        
/*        Handler.addQueueObjectsBuffered(turn);
        Handler.addQueueObjectsBuffered(move);*/
    }
    
    public float square(float f) {
        return f * f;
    }
    
    public float sqrt(float f) {
        return (float) Math.sqrt(f);
    }
    
    boolean turn = true;
    
    int counter = 0;
    
    @Override
    public boolean complete() {
        //System.out.println(counter++);
        if(!angleCalculated) {
            //System.out.println(Handler.angleDeg);
            angleCalculated = true;
            float deltaX = x - Handler.x, deltaY = y - Handler.y;
            
            float angle = 0;
            if(deltaX == 0 && deltaY == 0) {
                angle = Handler.angleDeg;
            }else if(deltaX == 0) {
                angle = deltaY > 0 ? 90 : 270;
            }else if(deltaY == 0) {
                angle = deltaX > 0 ? 0 : 180;
            }else if(deltaX > 0 && deltaY > 0) {
                angle = atan(deltaY / deltaX);
            }else if(deltaX < 0 && deltaY > 0) {
                angle = 180 - atan(deltaY / deltaX);
            }else if(deltaX < 0 && deltaY < 0) {
                angle = 180 + atan(deltaY / deltaX);
            }else if(deltaX > 0 && deltaY < 0){
                angle = 360 - atan(deltaY / deltaX);
            }
            
            this.targetAngle = angle;
        }
        
        //System.out.println("(" + Handler.x + ", " + Handler.y + ")");
        //System.out.println(Handler.angleDeg + " (" + Handler.x + ", " + Handler.y + ")");
        if(turn) {
            float delta = Handler.turnInternal(1);
            
            if(aboutEquals(Handler.angleDeg, targetAngle)) {
                turn = false;
                targetDist = sqrt(square(x - Handler.x) + square(y - Handler.y));
                //System.out.println(x + " " + Handler.x + " " + y + " " + Handler.y);
            }
        }else{
            if(currentDist < targetDist) {
                //System.out.println(Handler.angleDeg);
                float delta = prototype.setMovePower(1);
                Handler.updatePolarCartesianDelta(delta);
                currentDist += delta;
            }else{
                //System.out.println(x + " " + Handler.x + " " + y + " " + Handler.y);
                prototype.stopMotorPower();
                return true;
            }
        }
        return false;
        //return turn.complete() && move.complete();
    }
    
    public boolean aboutEquals(float x, float y) {
        return Math.abs(x - y) <= 1f;
    }

    @Override
    public void executeFrame() {
        //System.out.println("Executing");
        //System.out.println(Handler.x + " " + Handler.y);
        return;
    }

    @Override
    public void add(Operation o) {
        return;
    }
    
    public float pythag(float x, float y) {
        return (float)Math.sqrt(x * x + y * y);
    }
    
    //Rational number
    public float atan(float r) {
        return (float) Math.toDegrees(Math.atan(Math.abs(r)));
    }

}
