/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rover.ruckus.operations;

/**
 *
 * @author kenny ge
 */
public interface Operation {
    
    public abstract boolean complete();
    
    public abstract void executeFrame();
    
    public abstract void add(Operation o);
    
}
