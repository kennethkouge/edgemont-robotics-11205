package rover.ruckus.operations;

import rover.ruckus.prototype.*;
import rover.ruckus.operations.*;

public class PlaceMarkerOperation implements Operation {
    
    public boolean everComplete = false;
    public Prototype prototype;
    
    public PlaceMarkerOperation(Prototype prototype){
        this.prototype = prototype;
    }
    
    @Override
    public boolean complete() {
        return everComplete;
    }

    @Override
    public void executeFrame() {
        if(everComplete)
            return;
        everComplete |= prototype.placeMarker();
    }

    @Override
    public void add(Operation o) {
        
    }

    
    
}
