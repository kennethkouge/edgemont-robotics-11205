/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rover.ruckus.prototype;

import rover.ruckus.prototype.*;

import rover.ruckus.operations.Move;
import rover.ruckus.operations.Operation;
import rover.ruckus.operations.OperationSequence;
import rover.ruckus.operations.OperationSet;
import rover.ruckus.operations.Turn;

public class Handler {

    /**The operation queue. Thias acts almost like a line.*/
    public static OperationSequence queue;
    /**An instance of the object to be implemented by the builders once they're done*/
    public static Prototype prototype;
    
    /**The robot's current x and y position*/
    public static float x, y;
    /**
     * The x-axis is 0 degrees, positive is counterclockwise
     */
    public static float angleDeg;
    
    /**The method to be called before this handler is used. This is so that we can position
     * the robot and place it however we need to and pass it through the code here*/
    public static void create(Prototype p, float x, float y, float angleDeg) {
        Handler.x = x;
        Handler.y = y;
        Handler.angleDeg = angleDeg;
        queue = new OperationSequence();
        prototype = p;
        //Uncomment if necessary
        prototype.calibrate();
    }

    /**Stops the current action*/
    public static void cancelCurrentAction() {
        queue.queue.remove();
    }


    public static int frame = 0;
    /**For lack of a better term, this is called many times per second depending on the timing*/
    public static void callFrame() {
        Logger.addData("Frame", frame++);
        while(angleDeg < 0)
            angleDeg += 360;
        angleDeg %= 360;
        
        //System.out.println(angleDeg);
        
        //System.out.println(angleDeg);
        if(!queue.isEmpty()){
            Operation o = queue.queue.peek();
            o.executeFrame();
            if(o.complete()){
                queue.queue.remove();
                callFrame();
                return;
            }
        }
        
        try{
            Thread.sleep(1000 / 60);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public static void addQueueObjectsSimultaneous(Operation... o) {
        if (queue.isEmpty()) {
            OperationSet set = new OperationSet();
            queue.add(set);
        }
        for (Operation o2 : o) {
            queue.queue.peek().add(o2);
        }
    }
    
    /**Adds the following objects such that they run with the last set of simultaneous operations added */
    public static void addQueueObjectsLastBuffer(Operation...o) {
        if (queue.isEmpty()) {
            OperationSet set = new OperationSet();
            queue.add(set);
        }
        for (Operation o2 : o) {
            queue.queue.peekLast().add(o2);
        }
    }

    public static void addQueueObjectsBuffered(Operation... o) {
        OperationSet set = new OperationSet();
        for (Operation o2 : o) {
            set.add(o2);
        }
        queue.add(set);
    }

    public static void addQueueObjects(boolean simultaneous, Operation... o) {
        if (simultaneous) {
            addQueueObjectsSimultaneous(o);
        } else {
            addQueueObjectsBuffered(o);
        }
    }

    /**
     * @param degrees - the angle measure in degrees (counterclockwise is positive)
     * @param simultaneous - whether or not this action will be done while the other
     * operations are also
     */
    public static void turn(boolean simultaneous, float degrees) {
        Handler.addQueueObjects(simultaneous, new Turn(degrees, prototype));
    }

    /**
     * Returns the float value of the inverse tan function in degrees
     */
    public static float atan(float ratio) {
        return (float) Math.toDegrees(Math.atan(ratio));
    }

    /**
     * @param xFinal - the x coordinate to go to in the range [-1, 1]
     * @param yFinal - the y coordinate to go to in the range [-1, 1] Note that
     * this is a blocking command! This means that, until the current command
     * finishes, the robot can do nothing else! If you want me to add in the
     * feature to do other things, please let me know and give me the source
     * code so I can implement it. That task is non-trivial.
     */
    public static void moveToCoord(float xFinal, float yFinal) {
        float deltaX = xFinal - x;
        float deltaY = yFinal - y;

        float theta = -1;

        if (deltaX == 0 && deltaY == 0) {
            return;
        } else if (deltaX == 0) {
            theta = deltaY < 0 ? 270 : 90;
        } else if (deltaY == 0) {
            theta = deltaX < 0 ? 180 : 0;
        }

        float xAbs = Math.abs(deltaX);
        float yAbs = Math.abs(deltaY);

        if (deltaX > 0 && deltaY > 0) { //first quadrant
            theta = atan(yAbs / xAbs);
        } else if (deltaX > 0 && deltaY < 0) {
            theta = 270 + atan(xAbs / yAbs);
        } else if (deltaY > 0) { //don't need to check deltaX again because we know it is negative
            theta = 90 + atan(xAbs / yAbs);
        } else {
            theta = 180 + atan(yAbs / xAbs);
        }

        //It doesn't matter if they're negative because squaring removes the sign
        float deltaXCm = deltaX * 152.4f;
        float deltaYCm = deltaY * 152.4f;

        float hypotenuse = pythag(deltaXCm, deltaYCm);

        setAngle(false, theta);
        move(false, hypotenuse);
    }

    public static float pythag(float x, float y) {
        return (float) Math.sqrt(x * x + y * y);
    }

    public static void setAngle(boolean simultaneous, float angleDeg) {
        float delta = angleDeg - Handler.angleDeg;
        while (delta < 0) {
            delta += 360;
        }
        turn(simultaneous, delta % 360f);
    }

    /*public static void moveForward(boolean simultaneous, float cm) {
        float xCm = (float) (Math.cos(Math.toRadians(angleDeg)) * cm);
        float yCm = (float) (Math.sin(Math.toRadians(angleDeg)) * cm);

        float xCoord = xCm / Constants.ARENA_SIZE;
        float yCoord = yCm / Constants.ARENA_SIZE;

        x += xCoord;
        y += yCoord;

        move(simultaneous, cm);
    }*/
    
    public static void move(boolean simultaneous, float cm) {
        if(!simultaneous)
            addQueueObjectsBuffered(new Move(cm, prototype));
        else
            addQueueObjects(simultaneous, new Move(cm, prototype));
    }
    
    public static float sin(float degrees) {
        return (float) Math.sin(Math.toRadians(degrees));
    }
    
    public static float cos(float degrees) {
        return (float) Math.cos(Math.toRadians(degrees));
    }
    
    public static void updatePolarCartesianDelta(float dist){
        //float dy = (float) Math.sin(angleDeg) * dist;
        //float dx = (float) Math.cos(angleDeg) * dist;
        
        float dy = 0, dx = 0;
        
        if(angleDeg < 90) {
            dy = sin(angleDeg) * dist;
            dx = cos(angleDeg) * dist;
        }else if(angleDeg == 90) {
            dy = dist;
        }else if(angleDeg > 90 && angleDeg < 180) {
            float a = 180 - angleDeg;
            dy = sin(a) * dist;
            dx = -cos(a) * dist;
        }else if(angleDeg == 180) {
            dx = -dist;
        }else if(angleDeg > 180 && angleDeg < 270) {
            float a = angleDeg - 180;
            dy = -sin(a) * dist;
            dx = -cos(a) * dist;
        }else if(angleDeg == 270) {
            dy = -dist;
        }else if(angleDeg > 270) {
            float a = 360 - angleDeg;
            dy = -sin(a) * dist;
            dx = cos(a) * dist;
        }else if(angleDeg == 0 || angleDeg == 360) {
            dx = dist;
        }
        
        x += dx;
        y += dy;
    }

    public static float turnInternal(float signum) {
        float delta = prototype.setTurnPower(signum);
        angleDeg += delta;
        return delta;
    }
}
