package rover.ruckus.prototype;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.qualcomm.robotcore.hardware.HardwareMap;
import rover.ruckus.operations.MoveToPosition;
import rover.ruckus.operations.*;
import rover.ruckus.operations.SetAngle;
import rover.ruckus.prototype.Handler;
import rover.ruckus.prototype.Prototype;



/**
 *
 * @author kenny ge
 */
public class Main {
    
    public Prototype prototype;
    
    public static boolean running = true;
    private HardwareMap hardwareMap;
    
    public Main(Prototype p, double x, double y, double angleDeg, int mode, HardwareMap hardwareMap,
    int goldPos) {
        this.goldPos = goldPos;
        this.hardwareMap = hardwareMap;        
        while(angleDeg < 0)
            angleDeg += 360;
        angleDeg %= 360;
        
        Handler.create(p, (float)x, (float)y, (float)angleDeg);
        prototype = p;
        init(mode);
    }
    
    public int goldPos;
    
    public static final int RED_BOTTOM = 0, RED_TOP = 1, BLUE_BOTTOM = 2, BLUE_TOP = 3, TEST = 4;
    
    public int currentMode;
    
    public void init(int mode){
        this.currentMode = mode;
        /**Note to builders: Please uncomment this line and fill in the name
         of your subclass!!!*/
        //prototype = new ImplementedPrototype();
        //2nd red clockwise       
        prototype.calibrate();
        prototype.detach();
        
        if(currentMode == -1)
            Handler.addQueueObjectsBuffered(new Turn(360, prototype));
        else
            Handler.addQueueObjectsBuffered(new Turn(goldPos == 0 ? (-30) : (goldPos == 2 ? (30) : 0), prototype));
        
        switch(mode) {
            case TEST:
                
                
                //Handler.addQueueObjectsBuffered(new MoveGoldOperation(prototype, hardwareMap));
                //Handler.addQueueObjectsBuffered(new Move(70, prototype));
                //Handler.addQueueObjectsBuffered(new Turn(90, prototype));
                //Handler.addQueueObjectsBuffered(new Move(30, prototype));
                //Handler.addQueueObjectsBuffered(new BlankOperation(prototype));
                //Handler.addQueueObjectsBuffered(new Turn(90, prototype));
                //Handler.addQueueObjectsBuffered(new Turn(-90, prototype));
                break;
        case RED_TOP:
            Handler.addQueueObjectsBuffered(new MoveToPosition(20, -30, prototype));
            Handler.addQueueObjectsBuffered(new SetAngle(360 - 45, prototype));
        case RED_BOTTOM:
            Handler.addQueueObjectsBuffered(new Move(100, prototype));
            
            Handler.addQueueObjectsBuffered(new MoveToPosition(30, -30, prototype));
            
            Handler.addQueueObjectsBuffered(new Move(30, prototype));
            Handler.addQueueObjectsBuffered(new Turn(90, prototype));
            Handler.addQueueObjectsBuffered(new Move(130, prototype));
            Handler.addQueueObjectsBuffered(new Turn(45, prototype));
            Handler.addQueueObjectsBuffered(new Move(110, prototype));
            
            Handler.addQueueObjects(false, new PlaceMarkerOperation(prototype));
            
            Handler.addQueueObjectsBuffered(new Turn(90, prototype));
            
            Handler.addQueueObjectsBuffered(new Move(25, prototype));
            
            Handler.addQueueObjectsBuffered(new Turn(55, prototype));
            
            Handler.addQueueObjects(false, new MoveGoldOperation(prototype, hardwareMap));
            
            Handler.addQueueObjectsBuffered(new Move(100, prototype));
            
            Handler.addQueueObjectsBuffered(new MoveToPosition(150, 150, prototype));
            
            Handler.addQueueObjectsBuffered(new SetAngle(270, prototype));
            
            Handler.addQueueObjectsBuffered(new Move(140, prototype));
            
            Handler.addQueueObjectsBuffered(new MoveToPosition(60, -72, prototype));
            
            Handler.addQueueObjectsBuffered(new SetAngle(270 + 45, prototype));
            
            Handler.addQueueObjects(false, new MoveToPosition(60, -70, prototype));
            
            Handler.addQueueObjects(false, new MoveToPosition(160, 0, prototype));
            Handler.addQueueObjects(false, new SetAngle(270, prototype));
            
            Handler.addQueueObjectsBuffered(new Move(90, prototype));
            Handler.addQueueObjectsBuffered(new BlankOperation(prototype));
            break;
        case BLUE_TOP:
            Handler.addQueueObjectsBuffered(new MoveToPosition(-17, -25, prototype));
            Handler.addQueueObjectsBuffered(new SetAngle(180 + 45, prototype));
        case BLUE_BOTTOM:
            Handler.addQueueObjectsBuffered(new MoveToPosition(-60, -60, prototype));
            Handler.addQueueObjectsBuffered(new SetAngle(180 + 45, prototype));
            Handler.addQueueObjectsBuffered(new MoveGoldOperation(prototype, hardwareMap));
            Handler.addQueueObjectsBuffered(new MoveToPosition(-160, -160, prototype));
            Handler.addQueueObjectsBuffered(new PlaceMarkerOperation(prototype));
            Handler.addQueueObjectsBuffered(new MoveToPosition(-156, -91, prototype));
            Handler.addQueueObjectsBuffered(new MoveToPosition(-153, -3, prototype));
            Handler.addQueueObjectsBuffered(new MoveToPosition(-62.5f, 62, prototype));
            Handler.addQueueObjectsBuffered(new SetAngle(90 + 45, prototype));
            Handler.addQueueObjectsBuffered(new MoveGoldOperation(prototype, hardwareMap));
            Handler.addQueueObjectsBuffered(new Move(50, prototype));
            Handler.addQueueObjectsBuffered(new BlankOperation(prototype));
            break;
        }
        
        //Move the marker closer to the starting position
    }
    
    public void update(){
        Logger.addData("Mode", currentMode);
        Handler.callFrame();
        //Uncomment if necessary - refresh rate of 60Hz
        /*try{
          Thread.sleep(1000f / 60f);  
        }catch(Exception e){
            e.printStackTrace();
        }*/
    }
    
    public void run() {
        update();
    }
}
