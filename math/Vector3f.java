package math;

public class Vector3f {

    public float r, g, b;
    
    public Vector3f(float r, float g, float b) {
        this.r = r;
        this.g = g;
        this.b = b;
    }
    
    public Vector3f(Vector3f v) {
        this(v.r, v.g, v.b);
    }
    
    public float length() {
        return (float) Math.sqrt(r * r + g * g + b * b);
    }
    
    public Vector3f sub(Vector3f other) {
        this.r -= other.r;
        this.g -= other.g;
        this.b -= other.b;
        return this;
    }
    
    public Vector3f mul(float f) {
        this.r *= f;
        this.g *= f;
        this.b *= f;
        return this;
    }
    
    @Override
    public String toString() {
        return "(" + r + ", " + g + ", " + b + ")";
    }
    
}
