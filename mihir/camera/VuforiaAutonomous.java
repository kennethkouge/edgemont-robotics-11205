package mihir.camera;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import java.util.List;
import org.firstinspires.ftc.robotcore.external.ClassFactory;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer.CameraDirection;
import org.firstinspires.ftc.robotcore.external.tfod.TFObjectDetector;
import org.firstinspires.ftc.robotcore.external.tfod.Recognition;
import com.qualcomm.robotcore.hardware.HardwareMap;
import rover.ruckus.operations.MoveToPosition;
import rover.ruckus.operations.PlaceMarkerOperation;
import rover.ruckus.operations.SetAngle;
import rover.ruckus.prototype.Handler;
import rover.ruckus.prototype.Prototype;
import org.firstinspires.ftc.teamcode.AutoOpMode;
import com.vuforia.CameraDevice;

@Autonomous(name = "Final Autonomous", group = "Final")
public class VuforiaAutonomous extends LinearOpMode {
    private static final String TFOD_MODEL_ASSET = "RoverRuckus.tflite";
    private static final String LABEL_GOLD_MINERAL = "Gold Mineral";
    private static final String LABEL_SILVER_MINERAL = "Silver Mineral";
    private VuforiaLocalizer vuforia;
    private TFObjectDetector tfod;
    private AutoOpMode auto;

    @Override
    public void runOpMode() {
        initVuforia();

        if (ClassFactory.getInstance().canCreateTFObjectDetector()) {
            initTfod();
        } else {
            throw new UnsupportedOperationException("TFOD not supported");
        }

        waitForStart();
        int pos = -1; 
        if (opModeIsActive()) {
            if (tfod != null) {
                tfod.activate();
            }

            boolean detectedObject = false, done = false;
            
            int timer = 0;
            long start = System.currentTimeMillis();
            while((System.currentTimeMillis() - start) < 1000 * 10 && !detectedObject) {
                if (tfod != null) {

                    List<Recognition> updatedRecognitions = tfod.getUpdatedRecognitions();
                    if (updatedRecognitions != null) {
                      telemetry.addData("# Object Detected", updatedRecognitions.size());
                      if (updatedRecognitions.size() == 3 && !detectedObject) {
                        int goldMineralX = -1;
                        int silverMineral1X = -1;
                        int silverMineral2X = -1;
                        for (Recognition recognition : updatedRecognitions) {
                          if (recognition.getLabel().equals(LABEL_GOLD_MINERAL)) {
                            goldMineralX = (int) recognition.getLeft();
                          } else if (silverMineral1X == -1) {
                            silverMineral1X = (int) recognition.getLeft();
                          } else {
                            silverMineral2X = (int) recognition.getLeft();
                          }
                        }
                        if (goldMineralX != -1 && silverMineral1X != -1 && silverMineral2X != -1) {
                          if (goldMineralX < silverMineral1X && goldMineralX < silverMineral2X) {
                            telemetry.addData("Gold Mineral Position", "Left");
                            pos = 0;
                            detectedObject = true;
                          } else if (goldMineralX > silverMineral1X && goldMineralX > silverMineral2X) {
                            telemetry.addData("Gold Mineral Position", "Right");
                            detectedObject = true;
                            pos = 2;
                          } else {
                            telemetry.addData("Gold Mineral Position", "Center");
                            detectedObject = true;
                            pos = 1;
                          }
                        }
                      }
                      telemetry.update();
                    }
                }
            }
        }
        if (tfod != null) {
            tfod.shutdown();
        }
        
        java.util.Random random = new java.util.Random();
        
        if(pos == -1){
          //pos = random.nextInt(3);
        }
        
        telemetry.addData("Position", pos);
        telemetry.update();
        
        auto = new AutoOpMode(this, pos);
        
        while(opModeIsActive()){
          auto.loop();
        }
    }
    private void initVuforia() {
        //com.vuforia.Vuforia.init();
        VuforiaLocalizer.Parameters parameters = new VuforiaLocalizer.Parameters();
        parameters.vuforiaLicenseKey = "Ac2dH3v/////AAABmXnujNLwWET1jV6h2oMTxmRqw5GBwh3qWVXmWIG47cUEr4jzZVlDqLL8FXqzKaTp6HYlyaUbe/hwUJnbGTXIxag/QAu1eZvawWl7WDuA44J4gKgvpU3P05k/LIH59244uMjJxeCLk77lID8PLqZr8qgPePnZ69HUo+gnFdFmhaMrfF5qsNDkCRjr7ZhIYQFKEnwQfNt5OoPUpSZ1KyhEGyhJBgmZ3o3uGvbwF7V2l4LjM/RbFUlm5Aut2Yv03KDPm7A6LBbLLx3ttKMW6E0UYyyH3Z8SI2U7Cm4Qr2OPfTQNKgKISMuBtGDl3ok4MucxClIpcYyULOYd+sZMHGFAL3GjMOphEyjag4jZIYvllDb0";
        parameters.cameraDirection = CameraDirection.BACK;
        //CameraDevice.getInstance().init();
        vuforia = ClassFactory.getInstance().createVuforia(parameters);
    }
    private void initTfod() {
        int tfodMonitorViewId = hardwareMap.appContext.getResources().getIdentifier(
            "tfodMonitorViewId", "id", hardwareMap.appContext.getPackageName());
        TFObjectDetector.Parameters tfodParameters = new TFObjectDetector.Parameters(tfodMonitorViewId);
        tfod = ClassFactory.getInstance().createTFObjectDetector(tfodParameters, vuforia);
        tfod.loadModelFromAsset(TFOD_MODEL_ASSET, LABEL_GOLD_MINERAL, LABEL_SILVER_MINERAL);
    }
}
